import { IoLogoGithub, IoLogoGitlab } from "react-icons/io5";
import { type IconType } from "react-icons";

export type GitProvider = {
    name: string;
    icon: IconType;
    type: string;
    getCommits: (repo: Repo) => Promise<number>;
    getDescription: (repo: Repo) => Promise<string | undefined>;
};

export type Repo = {
    name: string;
    type: GitProvider;
    username: string;
    private?: boolean;
};

export const GitHub: GitProvider = {
    name: "GitHub",
    icon: IoLogoGithub,
    type: "github",
    getCommits: async (repo: Repo) => {
        const response = await fetch(`https://api.github.com/repos/${repo.username}/${repo.name}/contributors?anon=true`);
        const contributors: {
        login: string;
        contributions: number;
        }[] = await response.json();
        return contributors.reduce((acc, contributor) => acc + contributor.contributions, 0);
    },
    getDescription: async (repo: Repo) => {
        const response = await fetch(`https://api.github.com/repos/${repo.username}/${repo.name}`);
        const repoData: {
        description?: string;
        } = await response.json();
        return repoData.description;
    }
};

export const GitLab: GitProvider = {
    name: "GitLab",
    icon: IoLogoGitlab,
    type: "gitlab",
    getCommits: async (repo: Repo) => {
        const id = `${repo.username}%2F${repo.name}`;
        if (repo.private) {
        const response = await fetch(`https://dw-git-meta.vercel.app/repo/${id}/contributors`);
        const data: {
            numCommits: number
        } = await response.json(); 
        return data.numCommits; 
        }
        const response = await fetch(`https://gitlab.com/api/v4/projects/${id}/repository/contributors?sort=desc`);
        const contributors: {
        name: string;
        email: string;
        commits: number;
        }[] = await response.json();
        return contributors.reduce((acc, contributor) => acc + contributor.commits, 0);
    },
    getDescription: async (repo: Repo) => {
        const response = await fetch(`https://gitlab.com/api/v4/projects/${repo.username}%2F${repo.name}`);
        const repoData: {
        description?: string;
        } = await response.json();
        return repoData.description;
    }
};

