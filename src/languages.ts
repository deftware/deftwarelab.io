import { SiTypescript, SiReact, SiSolid, SiPostgresql } from "react-icons/si";
import { FaJava, FaRust, FaC, FaGitAlt, FaSwift } from "react-icons/fa6";
import { type IconType } from "react-icons";

export type Language = {
    name: string;
    icon: IconType;
    url: string;
};
  
export const Rust: Language = {
    name: "Rust",
    icon: FaRust,
    url: "https://www.rust-lang.org/"
};

export const Java: Language = {
    name: "Java",
    icon: FaJava,
    url: "https://adoptium.net/"
};

export const TypeScript: Language = {
    name: "TypeScript",
    icon: SiTypescript,
    url: "https://www.typescriptlang.org/"
};

export const C: Language = {
    name: "CLang",
    icon: FaC,
    url: "https://clang.llvm.org/"
};

export const Swift: Language = {
    name: "Swift",
    icon: FaSwift,
    url: "https://swift.org/"
};

// Technologies

export const React: Language = {
    name: "React",
    icon: SiReact,
    url: "https://reactjs.org/"
};

export const Postgresql: Language = {
    name: "PostgreSQL",
    icon: SiPostgresql,
    url: "https://www.postgresql.org/"
};

export const SolidJS: Language = {
    name: "Solid",
    icon: SiSolid,
    url: "https://solidjs.com/"
};

export const Git: Language = {
    name: "Git",
    icon: FaGitAlt,
    url: "https://git-scm.com/"
};