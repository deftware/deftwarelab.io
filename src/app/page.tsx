"use client";

import { IoGitCommit, IoLink } from "react-icons/io5";
import { AiOutlineLoading } from "react-icons/ai";
import { useEffect, useState } from "react";
import { type Language, C, Java, Postgresql, 
  React, Rust, SolidJS, Swift, TypeScript } from "@/languages";
import { type Repo, GitLab } from "@/git";

type ProjectMeta = {
  name: string;
  description?: string;
  link?: string;
  tags?: string[];
  language: Language[];
  repo?: Repo;
};

const Loading = (props: {text: string}) => 
  <span>Loading {props.text}... <AiOutlineLoading className="animate-spin inline pl-1" /></span>;

const GenericProject = (props: ProjectMeta) => {
  const [commits, setCommits] = useState<number>();
  const [description, setDescription] = useState<string | undefined>(props.description);
  useEffect(() => {
    if (props.repo) {
      props.repo.type.getCommits(props.repo).then(setCommits);
      if (!props.description) {
        props.repo.type.getDescription(props.repo).then(setDescription);
      }
    }
  }, [props]);
  return (
    <div className="rounded transition-colors hover:bg-zinc-700 bg-zinc-800 p-4 border-[1px] border-zinc-500">
      <h2 className="text-xl">{props.name}</h2>
      <h3 className="text-sm pb-2">{description ?? <Loading text="description" />}</h3>
      <div className="flex flex-wrap gap-2 text-sm">
        {props.repo && <>
          {props.link && <a className="hover:text-purple-400 transition-colors" href={`https://${props.link}`} target="_blank">
            {<IoLink className="inline" />} {props.link}
          </a>}
          {props.repo.private !== true && <a className="hover:text-purple-400 transition-colors" 
            href={`https://${props.repo.type.type}.com/${props.repo.username}/${props.repo.name}`} 
            target="_blank">
            {<props.repo.type.icon className="inline" />} {props.repo.type.type}/{props.repo.username}/{props.repo.name}
          </a>}
          <p>
            <IoGitCommit className="inline" />
            <span className="pl-1">{commits ?? "??"} commits</span>
          </p>
        </>}
        <p>
          {props.language.map((lang, i) => <span key={i}>
            <lang.icon className="inline" />
            <span className="pl-1">{lang.name}</span>
            {i < props.language.length - 1 && <span> / </span>}
          </span>)}   
        </p>
      </div>
      {props.tags && <div className="flex flex-wrap gap-2 mt-2">
        {props.tags.map((tag, index) => <span key={index} className="rounded px-2 py-[2px] bg-zinc-300 text-black">
          {tag}
        </span>)}
      </div>}
    </div>
  );
};

const Experience = (props: {languages: Language[]}) => (
  <div className="flex flex-wrap gap-2 mt-2">
    {props.languages.map((lang, index) => <a
    href={lang.url} target="_blank" rel="noreferrer" 
    key={index} className="rounded px-2 py-1 bg-zinc-800 hover:bg-zinc-700 transition-colors">
      <lang.icon className="inline" /> {lang.name}
    </a>)}
  </div>
);

const Heading = (props: {text: string, sub?: string}) => (
  <div className="my-4 border-l-2 border-zinc-500 pl-2">
    <h2 className="text-2xl">{props.text}</h2>
    {props.sub && <p className="text-sm">{props.sub}</p>}
  </div>
);

export default function Home() {
  return (
    <main className="w-full h-full flex justify-center items-center p-4 select-none">
      <div className="max-w-2xl w-full">
      <h1 className="text-3xl mt-4">Portfolio</h1>
        <p>
          My name is Alex, and this is a list of various projects I&apos;ve made over the years.
        </p>
        <div className="mt-2 w-fit">
          <Heading text="Experience" />
          <Experience languages={[Java, TypeScript, Rust, C, Swift, Postgresql]} />
          <div className="h-[1px] my-2 bg-zinc-500" />
          <Experience languages={[React, SolidJS, ]} />
          <p className="pt-1">...and the tools surrounding them.</p>
        </div>
        <Heading text="Highlighted Public Projects" sub="Open source projects I have worked on." />
        <div className="gap-2 flex flex-col mt-2">
          {/* Java */}
          <h2 className="text-sm">Java</h2>
          <GenericProject 
            name="EMC"
            language={[Java]}
            repo={{
              name: "EMC",
              username: "EMC-Framework",
              type: GitLab
            }}
            tags={["Minecraft", "CI/CD", "Mixin"]}
          />
          <GenericProject 
            name="Gradle Dependency Mocking"
            language={[Java]}
            repo={{
              name: "mocking",
              username: "deftware",
              type: GitLab
            }}
            tags={["Bytecode Generation", "ObjectWeb ASM"]}
          />
          <GenericProject 
            name="Aristois Installer"
            language={[Java]}
            repo={{
              name: "Installer",
              username: "Aristois",
              type: GitLab
            }}
            tags={["LWJGL"]}
          />
          <GenericProject 
            name="Minecraft Web Browser"
            language={[Java]}
            repo={{
              name: "mc-webbrowser",
              username: "deftware",
              type: GitLab
            }}
            tags={["Playwright", "OpenGL"]}
          />
          {/* Rust */}
          <h2 className="text-sm">Rust</h2>
          <GenericProject 
            name="Craft Test"
            language={[Rust]}
            repo={{
              name: "craft_test",
              username: "deftware",
              type: GitLab
            }}
            tags={["XVFB", "Testing", "CI/CD"]}
          />
          <GenericProject 
            name="Auth Server"
            language={[Rust]}
            repo={{
              name: "oauth-server",
              username: "deftware",
              type: GitLab
            }}
            tags={["Protocol", "Authentication", "Encryption"]}
          />
          <GenericProject 
            name="ACME Cli"
            language={[Rust]}
            repo={{
              name: "acme-cli",
              username: "deftware",
              type: GitLab
            }}
            tags={["ACME", "Automation", "CloudFlare"]}
          />
          {/* TypeScript */}
          <h2 className="text-sm">TypeScript</h2>
          <GenericProject 
            name="Portfolio Website"
            language={[TypeScript, React]}
            repo={{
              name: "deftware.gitlab.io",
              username: "deftware",
              type: GitLab
            }}
            tags={["NextJS 14", "Vercel", "Serverless"]}
          />
        </div>
        <Heading text="Highlighted Private Projects" sub="Non open-source projects I have worked on." />
        <div className="gap-2 flex flex-col mt-2">
          <GenericProject 
            name="Aristois"
            language={[Java]}
            description="All-in-one Minecraft utility mod using EMC."
            link="aristois.net"
            tags={["Minecraft", "Modding"]}
            repo={{
              name: "Aristois-EMC",
              type: GitLab,
              username: "Aristois",
              private: true
            }}
          />
          <GenericProject 
            name="Aristois Website"
            language={[TypeScript, SolidJS]}
            description="A website for the Aristois mod, made using SolidJS."
            link="aristois.net"
            repo={{
              name: "website",
              type: GitLab,
              username: "Aristois",
              private: true
            }}
            tags={["Redis", "PostgreSQL"]}
          />
          <GenericProject 
            name="SendGrid Discord Relay"
            language={[TypeScript]}
            description="An email to discord relay bot for SendGrid."
          />
          <GenericProject 
            name="IRC Server"
            language={[TypeScript]}
            description="Privacy focused high throughput IRC server."
          />
        </div>
      </div>
    </main>
  );
}
